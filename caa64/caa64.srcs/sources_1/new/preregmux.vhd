library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity preregmux is
    port (
        -- inputs
        data_i :  in std_logic_vector(64-1 downto 0); -- Data Input
        clk_i  :  in std_logic; -- Clock
        en_i   :  in std_logic; -- Enable
        rst_i  :  in std_logic; -- Reset
        -- outputs
        data_o : out std_logic_vector(32-1 downto 0);  -- Data Output (registered Data)
        en_o   : out std_logic -- Registered Enable 
        );
end entity preregmux;
    
architecture arch of preregmux is
    signal data_r   : std_logic_vector(data_i'range);
    signal en_r     : std_logic;
    signal sel_r    : std_logic;
begin
        
    reg_p: process (clk_i)
    begin
        if (rst_i = '1') then
            data_r <= (others=>'0');
            en_r   <= '0';
            sel_r  <= '0';
        elsif rising_edge(clk_i) then
            if (en_i='1')then
                if (sel_r = '0') then
                    data_r <= data_i;
                end if;
                sel_r  <= not sel_r;
            else
                sel_r  <= '0';
            end if;
            en_r   <= en_i;
        end if;
    end process;
        
    data_o <= data_r(2*data_o'length-1 downto 1*data_o'length) when sel_r = '1' else
              data_r(1*data_o'length-1 downto 0*data_o'length);
    en_o   <= en_r;
            
end architecture arch;
/*
 * verbose.h
 *
 *  Created on: 31/01/2020
 *      Author: mlabadm
 */

#ifndef SRC_VERBOSE_H_
#define SRC_VERBOSE_H_

#include <stdbool.h>

int verbose(const char * restrict, ...);
void setVerbose(bool);

#endif /* SRC_VERBOSE_H_ */

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity balanxor0 is
    generic (
        WIDTH_I : positive := 400 ; -- Input Data width (bits)
        WIDTH_O : positive :=  32 ; -- Output Data width (bits)
        WSIZE   : positive :=   8   -- Max Window and Apan for Reduced Xor (bits)
    );
    port (
        -- inputs
        data_i :  in std_logic_vector(WIDTH_I-1 downto 0); -- Data Input
        wsel_i :  in std_logic_vector(      3-1 downto 0); -- Windows Select (ceil(log2(WSIZE+1)))
        ssel_i :  in std_logic_vector(      3-1 downto 0); -- Select Select (ceil(log2(WSIZE+1)))
        clk_i  :  in std_logic                           ; -- Clock
        en_i   :  in std_logic                           ; -- Enable
        -- outputs
        data_o : out std_logic_vector(WIDTH_O-1 downto 0); -- Data Output
        en_o   : out std_logic                             -- Registered Enable 
   );
    end entity balanxor0;

    architecture ver_00 of balanxor0 is

    function xor_reduce(s: std_logic_vector) return std_logic is
        variable r: std_logic := '0';
    begin
        for i in s'range loop
            r := r xor s(i);
        end loop;
        return r;
    end function;

    constant WS : integer := WSIZE-1;
    constant SS : integer := WSIZE-1;
    
    subtype t_dim1 is std_logic_vector(WIDTH_I-1 downto 0);
    type    t_dim1_vector is array(natural range <>) of t_dim1;
    subtype t_dim2 is t_dim1_vector(0 to SS);
    type    t_dim2_vector is array(natural range <>) of t_dim2;
    subtype t_dim3 is t_dim2_vector(0 to WS);

    signal data_r   : std_logic_vector(WIDTH_I-1 downto 0);
    signal wsel_int : natural range 0 to WS;
    signal ssel_int : natural range 0 to SS;
    signal wsel_ind : natural range 0 to WS;
    signal ssel_ind : natural range 0 to SS;
    signal en_r     : std_logic;
    signal xor_s    : t_dim3;
    
begin

    wsel_int <= to_integer(unsigned(wsel_i));
    ssel_int <= to_integer(unsigned(ssel_i));
    wsel_ind <= wsel_int when wsel_int<=WS else WS;
    ssel_ind <= ssel_int when ssel_int<=SS else SS;

    reg_p: process (clk_i)
    begin
        if rising_edge(clk_i) then
            if (en_i='1') then
                data_r <= data_i;
                en_r   <= '1';
            else
                data_r <= data_r;
                en_r   <= '0';
            end if;
        end if;
    end process;
    
  
    win_g: for w in 1 to WSIZE generate
    begin
        span_g: for s in 1 to WSIZE generate
        begin
            dir_g: if w=1 generate
            begin
                xor_s(w-1)(s-1) <= data_r;
            end generate dir_g;
            xor_g: if not (w=1) generate
            begin
                xorbit_g: for b in 0 to (WIDTH_I-w)/s generate
                    xor_s(w-1)(s-1)(b) <= xor_reduce(data_r(s*b+w-1 downto s*b));
                end generate xorbit_g;
            end generate xor_g;
        end generate span_g;
    end generate win_g;

    data_o <= xor_s(wsel_ind)(ssel_ind)(data_o'range);
    en_o   <= en_r;

end architecture ver_00;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity balanxor1_tb is
end entity balanxor1_tb;

architecture test of balanxor1_tb is
    -- Components
    component balanxor1 is
        port (
           -- inputs
           data_i :  in std_logic_vector(256-1 downto 0); -- Data Input
           sel_i  :  in std_logic_vector(  2-1 downto 0); -- Xor/Direct
           clk_i  :  in std_logic; -- Clock
           en_i   :  in std_logic; -- Enable
           -- outputs
           data_o : out std_logic_vector( 32-1 downto 0);  -- Data Output
           en_o   : out std_logic -- Registered Enable 
        );
    end component balanxor1;
    -- Simulation parameters
    constant PERIOD : time := 10 ns; -- Input Data width (bits)
    constant I0     : std_logic_vector(256-1 downto 0) := "0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101" ; 
    constant I1     : std_logic_vector(256-1 downto 0) := "1000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000111000" ; 
    -- simulation signals
    signal stop_s   : std_logic                        :=   '0' ; -- Stop simulation 
    -- inputs
    signal data_i : std_logic_vector(256-1 downto 0) :=   I0 ; -- Data Input
    signal sel_i  : std_logic_vector(  2-1 downto 0) := "00" ; -- Select
    signal clk_i  : std_logic                        := '0'  ; -- Clock
    signal en_i   : std_logic                        := '0'  ; -- Enable
    -- outputs
    signal data_o : std_logic_vector( 32-1 downto 0)        ; -- Data Output
    signal en_o   : std_logic                               ; -- Registered Enable 
begin
    -- Unity Under Test
    balanxor_uut: balanxor1
    port map (
        -- inputs
        data_i => data_i, -- Data Input
        sel_i  => sel_i ,
        clk_i  => clk_i , -- Clock
        en_i   => en_i  , -- Enable
        -- outputs
        data_o => data_o ,-- Data Output
        en_o   => en_o    -- Registered Enable 
    );
    -- Clock generator
    clk_p: process
    begin
        if stop_s = '0' then
            clk_i <= not clk_i;
            wait for PERIOD/2;
        else
            wait;
        end if;
    end process clk_p;
    -- Signal Assignment
    signal_p: process
    begin
        wait for 5*PERIOD ;
        en_i   <= '1' ;
        wait for 2*PERIOD ;
        sel_i  <= "01" ;
        wait for 2*PERIOD ;
        en_i   <= '0' ;
        wait for 2*PERIOD ;
        sel_i  <= "10" ;
        wait for 2*PERIOD ;
        sel_i  <= "11" ;
        wait for 2*PERIOD ;
        sel_i  <= "00" ;
        wait for 2*PERIOD ;
        data_i <=  I1 ;
        wait for 2*PERIOD ;
        en_i   <=  '1' ;
        wait for 2*PERIOD ;
        sel_i  <= "00" ;
        wait for 2*PERIOD ;
        sel_i  <= "01" ;
        wait for 2*PERIOD ;
        sel_i  <= "10" ;
        wait for 2*PERIOD ;
        sel_i  <= "11" ;
        wait for 2*PERIOD ;
        en_i   <=  '0' ;
        wait for 2*PERIOD ;
        stop_s <= '1';
        wait;
    end process signal_p;
end architecture test;
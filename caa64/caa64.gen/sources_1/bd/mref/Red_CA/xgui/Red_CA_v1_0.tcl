# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "N_cel" -parent ${Page_0}
  ipgui::add_param $IPINST -name "i_N" -parent ${Page_0}


}

proc update_PARAM_VALUE.N_cel { PARAM_VALUE.N_cel } {
	# Procedure called to update N_cel when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.N_cel { PARAM_VALUE.N_cel } {
	# Procedure called to validate N_cel
	return true
}

proc update_PARAM_VALUE.i_N { PARAM_VALUE.i_N } {
	# Procedure called to update i_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.i_N { PARAM_VALUE.i_N } {
	# Procedure called to validate i_N
	return true
}


proc update_MODELPARAM_VALUE.N_cel { MODELPARAM_VALUE.N_cel PARAM_VALUE.N_cel } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.N_cel}] ${MODELPARAM_VALUE.N_cel}
}

proc update_MODELPARAM_VALUE.i_N { MODELPARAM_VALUE.i_N PARAM_VALUE.i_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.i_N}] ${MODELPARAM_VALUE.i_N}
}


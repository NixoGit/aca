-- Different connections at the mux input
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity preregmux3 is
    port (
        -- inputs
        data_i :  in std_logic_vector(64-1 downto 0); -- Data Input
        clk_i  :  in std_logic; -- Clock
        en_i   :  in std_logic; -- Enable
        rst_i  :  in std_logic; -- Reset
        -- outputs
        data_o : out std_logic_vector(32-1 downto 0);  -- Data Output (registered Data)
        en_o   : out std_logic -- Registered Enable 
        );
end entity preregmux3;
    
architecture arch of preregmux3 is
    signal data_r   : std_logic_vector(data_i'range);
    signal data_sel : std_logic_vector(data_i'range);
    signal en_r     : std_logic;
    signal sel_r    : std_logic;
begin
        
    reg_p: process (clk_i)
    begin
        if (rst_i = '1') then
            data_r <= (others=>'0');
            en_r   <= '0';
            sel_r  <= '0';
        elsif rising_edge(clk_i) then
            if (en_i='1')then
                if (sel_r = '0') then
                    data_r <= data_i;
                end if;
                sel_r  <= not sel_r;
            else
                sel_r  <= '0';
            end if;
            en_r   <= en_i;
        end if;
    end process;
    
    fg_mux: for i in 0 to data_o'length/2-1 generate
        data_sel(i) <= data_r(2*i); -- first half <= even cells
        data_sel(i+data_o'length/2) <= data_r(2*i+1); -- second half <= odd cells
    end generate;
    
    data_o <= data_sel(2*data_o'length-1 downto 1*data_o'length) when sel_r = '1' else
              data_sel(1*data_o'length-1 downto 0*data_o'length);
    en_o   <= en_r;
            
end architecture arch;
/*
 * caa_ctrl.c
 *
 *  Created on: 10/11/2020
 *      Author: mlabadm
 */
#include <stdio.h>
#include <string.h>
#include "xil_io.h"
#include "verbose.h"
#include "comblock.h"


void strPasIn(char *cmd){
	cmd=strtok(NULL, ",");
	verbose("Rule:");
	verbose(cmd);
	verbose("\n\r");
	cmd=strtok(NULL, ",");
	verbose("fb_type: ");
	verbose(cmd);
	verbose("\n\r");
	cmd=strtok(NULL, ",");
	verbose("Sync: ");
	verbose(cmd);
	verbose("\n\r");
	cmd=strtok(NULL, ",");
	verbose("FIFO_READ: ");
	verbose(cmd);
	verbose("\n\r");
	cmd=strtok(NULL, ",");
	verbose("Clear: ");
	verbose(cmd);
	verbose("\n\r");
	cmd=strtok(NULL, ",");
	verbose("Set: ");
	verbose(cmd);
	verbose("\n\r");
	cmd=strtok(NULL, ",");
	verbose("Init: ");
	verbose(cmd);
	verbose("\n\r");
	cmd=strtok(NULL, ",");
	verbose("Test: ");
	verbose(cmd);
	verbose("\n\r");
	cmd=strtok(NULL, ",");
	verbose("Reset: ");
	verbose(cmd);
	verbose("\n\r");
};

void caa_setCfg(u32 rule, u32 fb_type, u32 sync, u32 clear, u32 set, u32 init, u32 test, u32 reset){
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG9, test); //Test Enable

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG8, rule); //Set Rule
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG0, fb_type);  //Set fb_type
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG1, sync);  //Set sync
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG2, clear);  //Clear
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG3, set);  //Set
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG4, init);  //Init

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, reset); //reset cell array


}

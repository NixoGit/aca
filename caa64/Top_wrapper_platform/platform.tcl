# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\caa64\Top_wrapper_platform\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\caa64\Top_wrapper_platform\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {Top_wrapper_platform} -hw {C:\caa64\Top_wrapper.xsa} -out {C:/caa64}
platform write
domain create -name {freertos10_xilinx_ps7_cortexa9_0} -display-name {freertos10_xilinx_ps7_cortexa9_0} -os {freertos10_xilinx} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {udma_server}
platform generate -domains 
platform active {Top_wrapper_platform}
domain active {zynq_fsbl}
domain active {freertos10_xilinx_ps7_cortexa9_0}
platform generate -quick
platform generate

# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct /opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_balanxor1_base_wrapper/platform.tcl
# 
# OR launch xsct and run below command.
# source /opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_balanxor1_base_wrapper/platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {caa_256_balanxor1_base_wrapper}\
-hw {/opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_balanxor1_base_wrapper.xsa}\
-out {/opt/my_gits/AUTOMATAS/automatas/Vitis}

platform write
domain create -name {freertos10_xilinx_ps7_cortexa9_0} -display-name {freertos10_xilinx_ps7_cortexa9_0} -os {freertos10_xilinx} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {udma_server}
platform generate -domains 
platform active {caa_256_balanxor1_base_wrapper}
domain active {zynq_fsbl}
domain active {freertos10_xilinx_ps7_cortexa9_0}
platform generate -quick
platform generate
platform config -updatehw {/opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_balanxor1_base_wrapper.xsa}
platform config -updatehw {/opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_balanxor1_base_wrapper.xsa}
platform generate -domains 
domain active {zynq_fsbl}
domain active {freertos10_xilinx_ps7_cortexa9_0}
bsp reload
bsp config tick_rate "1000"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
platform clean
platform clean
platform generate
bsp reload
bsp config ip_frag_max_mtu "15000"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
bsp config ip_frag_max_mtu "15000"
bsp reload
platform write
platform active {caa_256_balanxor1_base_wrapper}
bsp reload
bsp config lwip_debug "false"
bsp config netif_debug "false"
bsp config lwip_debug "true"
bsp config socket_debug "true"
bsp config tcp_debug "true"
bsp write
bsp reload
catch {bsp regenerate}
platform clean
bsp config socket_debug "true"
bsp config tcp_debug "false"
bsp config socket_debug "false"
bsp write
bsp reload
catch {bsp regenerate}
bsp config lwip_debug "false"
bsp config sys_debug "false"
bsp config tcp_debug "false"
bsp write
bsp reload
catch {bsp regenerate}
platform generate
platform clean
platform generate
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
domain active {zynq_fsbl}
domain active {freertos10_xilinx_ps7_cortexa9_0}
bsp reload
bsp reload
bsp config ip_frag_max_mtu "1500"
bsp reload
bsp config ip_frag_max_mtu "15000"
bsp config ip_frag "1"
bsp config ip_options "0"
bsp config ip_reass_max_pbufs "128"
bsp reload
platform generate -domains 
bsp config ip_frag "1"
bsp config ip_reassembly "1"
bsp reload
bsp reload
bsp config tick_rate "100"
bsp write
bsp reload
catch {bsp regenerate}
bsp reload
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
bsp reload
bsp reload
bsp reload
bsp reload
bsp config ip_options "0"
bsp config ip_options "1"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
platform clean
platform generate
platform clean
platform generate
bsp reload
bsp config ip_options "0"
bsp config ip_forward "1"
bsp write
bsp reload
catch {bsp regenerate}
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
bsp reload
bsp config ip_forward "0"
bsp config ip_frag_max_mtu "1500"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
platform clean
platform generate
platform clean
platform generate
bsp config ip_frag "1"
bsp config ip_reassembly "0"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
platform clean
platform generate
bsp config ip_reassembly "1"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
platform active {caa_256_balanxor1_base_wrapper}
catch {platform remove caa_256_balanxor1_base_wrapper_1}
catch {platform remove caa_256_balanxor1_base_wrapper_2}
platform clean
platform generate

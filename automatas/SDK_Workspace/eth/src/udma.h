#ifndef UDMA_H
#define UDMA_H

#include <stdio.h>
#include <string.h>
#include "comblock.h"

#define true 1
#define false 0

#include "xparameters.h"

#define READ_REG           0
#define READ_RAM           1
#define READ_MEM           2
#define READ_FIFO          3
/////////////////////////////////////
#define WRITE_REG          4
#define WRITE_RAM          5
#define WRITE_MEM          6
#define WRITE_FIFO         7
///////////////////////////////////
#define UDMA               8
#define LOG				   255
#define BUFF_SIZE          4096


/*TODO required module registers
ID:0
Size:1
Type:2
	operator
	storage
	router
	interface
MemType:3
	Fifo
	Dram

Write process:
	check if resource is available in case of specific commands
	check if memory is reachable in case of udma or mem
	check if width of word is compatible
		return success or fail

read process
	check if resource is available in case of specific commands
	check if memory is reachable in case of udma or mem
		return success or fail

0 -> Failure
1 -> Success
	*/

u32 logging = true;

static inline void read_FIFO(UINTPTR baseaddr, u32 *send_buf, u32 length) {
	volatile u32 i = 0;
	while((cbRead(baseaddr, CB_IFIFO_STATUS) & 0x01) == 0) {
		if (length > 0 && i == length)	// read until empty or a specified length
			break;
		send_buf[i + 1] = cbRead(baseaddr, CB_IFIFO_VALUE);
		i++;
	}
    if (cbRead(baseaddr, CB_IFIFO_STATUS) & 0x04) {
    	send_buf[0]= 2; // if underflow there was an error
    } else {
    	send_buf[0]= 1;
    }
}

static inline void write_FIFO(UINTPTR baseaddr, u32 *recv_buf, u32 length, u32 *send_buf) {
	volatile u32 i = 0;
	while((cbRead(baseaddr, CB_OFIFO_STATUS) & 0x01) == 0 && i < length) {
		cbWrite(baseaddr, CB_OFIFO_VALUE, recv_buf[i + 2]);
		i++;
	}
    if (cbRead(baseaddr, CB_OFIFO_STATUS) & 0x04) {
    	send_buf[0]= 2; // if overflow there was an error
    } else {
    	send_buf[0]= 1;
    }
}

static inline void read_RAM(UINTPTR baseaddr, UINTPTR offset, u32 *send_buf, u32 length, u32 inc) {
	if (inc == 1) {
		if(logging)
			xil_printf("Reading: %d, %d, %d \n", baseaddr + offset, length, inc);
		cbReadBulk((int *)(send_buf + 1), baseaddr + offset, length);
	} else {
		volatile u32 i;
		for (i = 0; i < length; i ++) {
			if (logging)
				xil_printf("Reading: %d, %d \n", baseaddr + offset, i * inc * 4);
			send_buf[i + 1]= cbRead(baseaddr + offset, i * inc);
		}
	}
	if(length * inc * 4 > XPAR_COMBLOCK_0_DRAM_IO_DEPTH - offset)
		send_buf[0]= 2; // if overflow there was an error
	else
		send_buf[0]= 1;
}

static inline void write_RAM(UINTPTR baseaddr, UINTPTR offset, u32 *recv_buf, u32 length, u32 inc, u32 *send_buf) {
	if (inc == 1) {
		if(logging)
			xil_printf("Writing: %d, %d \n", baseaddr + offset, (int *)(recv_buf + 5));
		cbWriteBulk(baseaddr + offset, (int *) (recv_buf + 5), length);
	}else {
		volatile u32 i;
		for (i = 0; i < length; i ++) {
			if(logging)
				xil_printf("Writing: %d, %d, %d \n", baseaddr + offset, i * 4 * inc, recv_buf[i + 5]);
			cbWrite(baseaddr + offset, i * inc, recv_buf[i + 5]);
		}
	}
	if(logging)
		xil_printf("Partial success \n");
	if(length * inc * 4 > XPAR_COMBLOCK_0_DRAM_IO_DEPTH - offset)
		send_buf[0]= 2; // if overflow there was an error
	else
		send_buf[0]= 1;
}

static inline void read_MEM(UINTPTR baseaddr, u32 *send_buf, u32 length, u32 inc) {
	if (inc == 1)
		memmove((UINTPTR *)baseaddr, send_buf + 1, length);
	else {
		volatile u32 i;
		for (i = 0; i < length; i ++) {
			send_buf[i + 1]= cbRead(baseaddr, i * inc);
		}
	}
	send_buf[0] = 1; // success always assumed, unprotected operations are fully responsibility of the user
}

static inline void write_MEM(UINTPTR baseaddr, u32 *recv_buf, u32 length, u32 inc, u32 *send_buf) {
	if (inc == 1)
		memmove((UINTPTR *)baseaddr, recv_buf, length);
	else {
		volatile u32 i;
		for (i = 0; i < length; i ++) {
			cbWrite(baseaddr, i * inc, recv_buf[i + 5]);
		}
	}
	send_buf[0] = 1; // success always assumed, unprotected operations are fully responsibility of the user
}

static inline void process_command(u32 *recv_buf, int sd) {
	u32 send_buf[BUFF_SIZE];
	send_buf[0]= 0; // always error unless a successful operation
	u32 pack_type = recv_buf[0];
	if(logging)
		xil_printf("\n------- Packet type:\t %d -------\n",pack_type);
	u32 o = 0;
	switch(pack_type){
		case READ_REG:
			if(logging)
				xil_printf("Register:\t %u \n", recv_buf[1]);
			if (XPAR_COMBLOCK_0_REGS_IN_ENA) {
				send_buf[1] = cbRead(XPAR_COMBLOCK_0_AXIL_BASEADDR, recv_buf[1]);
				send_buf[0] = 1;
				if(logging)
					xil_printf("READ VALUE:\t %u \n", send_buf[1]);
				write(sd, send_buf, 2 * 4);
			} else
				write(sd, send_buf, 4);
			break;
		case READ_RAM:
			if(logging)
				xil_printf("Address:\t %u N:\t %u Inc:\t %u\n",
						recv_buf[1], recv_buf[2], recv_buf[3]);
			if (XPAR_COMBLOCK_0_DRAM_IO_ENA) {
				read_RAM(XPAR_COMBLOCK_0_AXIF_BASEADDR, (UINTPTR) recv_buf[1] * 4,
					send_buf, recv_buf[2], recv_buf[3]);
				if(logging) {
					for(o = 0; o < recv_buf[2] + 1; o++)
						xil_printf("data: %u \n", send_buf[o]);
				}
				write(sd, send_buf, (recv_buf[2] + 1) * 4);
			} else
				write(sd, send_buf, 4);
			break;
		case READ_MEM:
			if(logging) {
				xil_printf("Warning, unprotected operation!");
				xil_printf("Address:\t %u N:\t %u Inc:\t %u\n",
						recv_buf[1], recv_buf[2], recv_buf[3]);
			}
			if(!(recv_buf[1] % 4))
				read_MEM((UINTPTR) recv_buf[1], send_buf, recv_buf[2], recv_buf[3]);
			else {
				if(logging)
					printf("Address must be a multiple of 4 due to the data width (4 bytes).\n");
			}
			if(logging) {
				for(o = 0; o < recv_buf[2] + 1; o++)
					xil_printf("data: %u \n", send_buf[o]);
			}
			write(sd, send_buf, (recv_buf[2] + 1) * 4);
			break;
		case READ_FIFO:
			if(logging)
				xil_printf("FIFO N:\t %u \n", recv_buf[1]);
			if (XPAR_COMBLOCK_0_FIFO_IN_ENA) {
				read_FIFO(XPAR_COMBLOCK_0_AXIL_BASEADDR, send_buf, recv_buf[1]);
				write(sd, send_buf, (recv_buf[1] + 1) * 4);
				if(logging) {
					for(o = 0; o < recv_buf[1] + 1; o++)
						xil_printf("data: %u \n", send_buf[o]);
				}
			} else
				write(sd, send_buf, 4);
			break;
		case WRITE_REG:
			if(logging)
				xil_printf("Register:\t %u data:\t %u \n",recv_buf[1],recv_buf[2]);
			if (XPAR_COMBLOCK_0_REGS_OUT_ENA) {
				cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, recv_buf[1] + 16, recv_buf[2]);
				send_buf[0] = 1;
			}
			write(sd, send_buf, 4);
			break;
		case WRITE_RAM:
			if(logging) {
				xil_printf("Addr:\t %u N:\t %u Inc:\t %u Rad:\t %u\n",
						recv_buf[1], recv_buf[2], recv_buf[3], recv_buf[4]);
				for(o = 0; o < recv_buf[2]; o++)
					xil_printf("data: %u \n", recv_buf[o + 5]);
			}
			if (XPAR_COMBLOCK_0_DRAM_IO_ENA) {
				if(logging)
					xil_printf("Resources available\n");
				write_RAM(XPAR_COMBLOCK_0_AXIF_BASEADDR, recv_buf[1] * 4,
					recv_buf, (UINTPTR) recv_buf[2], recv_buf[3], send_buf);
				if(logging)
					xil_printf("Operation completed with result %d \n", send_buf[0]);
			}
			write(sd, send_buf, 4);
			break;
		case WRITE_MEM:
			if(logging) {
				xil_printf("Warning, non protected operation!\n");
				xil_printf("Addr:\t %u N:\t %u Inc:\t %u Rad:\t %u\n",
						recv_buf[1], recv_buf[2], recv_buf[3], recv_buf[4]);
				for(o = 0; o < recv_buf[2]; o++)
					xil_printf("data: %u \n", recv_buf[o + 5]);
			}
			if(!(recv_buf[1] % 4))
				write_MEM(recv_buf[1], recv_buf, recv_buf[2], recv_buf[3], send_buf);
			else {
				if(logging)
					printf("Address must be a multiple of 4 due to the data width (4 bytes).\n");
			}
			write(sd, send_buf, 4);
			break;
        case WRITE_FIFO:
        	if(logging) {
        		xil_printf("FIFO N:\t %u \n", recv_buf[1]);
        		for(o = 0; o < recv_buf[1]; o++)
        			xil_printf("data: %u \n", recv_buf[o + 2]);
        	}
			if (XPAR_COMBLOCK_0_FIFO_OUT_ENA) {
				write_FIFO(XPAR_COMBLOCK_0_AXIL_BASEADDR, recv_buf, recv_buf[1], send_buf);
			}
			write(sd, send_buf, 4);
            break;
		case UDMA:
			// write in special memory for UDMA module to process
			break;
		case LOG:
			logging ^= true;
			if(logging) {
				xil_printf("Logging enabled. \n");
			}else{
				xil_printf("Logging disabled. \n");}
		default:
			break;

	}
}

#endif //UDMA_H
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity prereg is
   generic (
      WIDTH_I : positive:=128; -- Input Data width (bits)
      WIDTH_O : positive:=32   -- Output Data width (bits)
   );
   port (
      -- inputs
      data_i :  in std_logic_vector(WIDTH_I-1 downto 0); -- Data Input
      clk_i  :  in std_logic; -- Clock
      en_i   :  in std_logic; -- Enable
      -- outputs
      data_o : out std_logic_vector(WIDTH_O-1 downto 0);  -- Data Output (registered Data)
      en_o   : out std_logic -- Registered Enable 
   );
end entity prereg;

architecture arch of prereg is
   signal data_r   : std_logic_vector(data_i'range);
   signal en_r     : std_logic;
begin

   reg_p: process (clk_i)
   begin
      if rising_edge(clk_i) then
         data_r <= data_i;
         en_r   <= en_i;
      end if;
   end process;
   
   data_o <= data_r(data_o'range);
   en_o   <= en_r;

end architecture arch;
-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 15.3.2023 16:29:22 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_balancexor_array is
    generic(ncells : integer:=256;
                num_xor : integer:=8
                );
end tb_balancexor_array;

architecture tb of tb_balancexor_array is
    constant data_size : integer :=ncells/num_xor;

    component balancexor_array
        generic(ncells : integer:=ncells;
                num_xor : integer:=num_xor
                );
        port (data_i   : in std_logic_vector (ncells-1 downto 0);
              dvalid_i : in std_logic;
              en_i     : in STD_LOGIC;
              sel_i    : in std_logic;
              clk_i    : in std_logic;
              rst_n    : in std_logic;
              data_o   : out std_logic_vector (data_size-1 downto 0);
              dvalid_o   : out STD_LOGIC;
              dxor_dbg_o : out STD_LOGIC_VECTOR((ncells/num_xor) -1 downto 0));
    end component;

    signal data_i   : std_logic_vector (ncells-1 downto 0);
    signal dvalid_i : std_logic;
    signal en_i : std_logic;
    signal sel_i    : std_logic;
    signal clk_i    : std_logic;
    signal rst_n    : std_logic;
    signal data_o   : std_logic_vector (data_size-1 downto 0);
    signal dvalid_o : std_logic;
    signal dxor_dbg_o : std_logic_vector((ncells/num_xor) -1 downto 0);

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : balancexor_array
    port map (data_i   => data_i,
              dvalid_i => dvalid_i,
              en_i     => en_i,
              sel_i    => sel_i,
              clk_i    => clk_i,
              rst_n    => rst_n,
              data_o   => data_o,
              dvalid_o => dvalid_o,
              dxor_dbg_o => dxor_dbg_o
              );

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk_i is really your main clock signal
    clk_i <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        data_i <= (others => '0');
        dvalid_i <= '0';
        sel_i <= '1';
        en_i<='0';

        -- Reset generation
        -- EDIT: Check that rst_n is really your reset signal
        rst_n <= '0';
        wait for 100 ns;
        rst_n <= '1';
        en_i<='0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        data_i<=x"cf44a5526743e7c586f4128e3b0abe92fef552d691947fb21dd4b781b28bb7e2";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 20*TbPeriod;
        data_i<=x"88932c2707de4637a4f310c924eee4f5b28371c9d10ff4df7e668aad7e32c20e";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 20*TbPeriod;
        data_i<=x"221ec37af1a2c0366faa9378c9ca070d90c56976bed352e8a9e887573ccee3f9";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 19*TbPeriod;
        en_i<='1';
        wait for TbPeriod;
        data_i<=x"cf44a5526743e7c586f4128e3b0abe92fef552d691947fb21dd4b781b28bb7e2";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 20*TbPeriod;
        data_i<=x"88932c2707de4637a4f310c924eee4f5b28371c9d10ff4df7e668aad7e32c20e";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 20*TbPeriod;
        data_i<=x"221ec37af1a2c0366faa9378c9ca070d90c56976bed352e8a9e887573ccee3f9";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 20*TbPeriod;
        data_i<=x"3185d825248b5bdedaf3140f291645bea93a43a1d2d2845a9d964cbaea8502dd";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 20*TbPeriod;
        data_i<=x"3b57420ae4494a820639ddc915b47ffbce5779fac62efee05cf297aa30da99d5";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 20*TbPeriod;
        data_i<=x"89abc01234589abc01234589abc01234589abc01234589abc01234589abc0123";
        dvalid_i<='1';
        wait for TbPeriod;
        dvalid_i<='0';
        wait for 20*TbPeriod;
        

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;

-- Configuration block below is required by some simulators. Usually no need to edit.

-- configuration cfg_tb_balancexor_array of tb_balancexor_array is
--     for tb
--     end for;
-- end cfg_tb_balancexor_array;
# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct /home/agustin/Escritorio/Github/ACA/automatas-clock_div/SDK_Workspace/Top_wrapper/platform.tcl
# 
# OR launch xsct and run below command.
# source /home/agustin/Escritorio/Github/ACA/automatas-clock_div/SDK_Workspace/Top_wrapper/platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {Top_wrapper}\
-hw {/home/agustin/Escritorio/Github/ACA/automatas-clock_div/caa/Top_wrapper.xsa}\
-out {/home/agustin/Escritorio/Github/ACA/automatas-clock_div/SDK_Workspace}

platform write
domain create -name {freertos10_xilinx_ps7_cortexa9_0} -display-name {freertos10_xilinx_ps7_cortexa9_0} -os {freertos10_xilinx} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {freertos_lwip_echo_server}
platform generate -domains 
platform active {Top_wrapper}
domain active {zynq_fsbl}
domain active {freertos10_xilinx_ps7_cortexa9_0}
platform generate -quick
bsp reload
platform generate
bsp config dhcp_does_arp_check "false"
bsp config lwip_dhcp "false"
bsp config total_heap_size "65536"
bsp config total_heap_size "524288"
bsp write
bsp reload
catch {bsp regenerate}
platform generate -domains freertos10_xilinx_ps7_cortexa9_0 
platform active {Top_wrapper}
platform config -updatehw {/home/agustin/Escritorio/Github/ACA/automatas-clock_div/caa/Top_wrapper.xsa}
platform generate -domains 

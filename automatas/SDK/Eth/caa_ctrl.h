/*
 * caa_ctrl.h
 *
 *  Created on: 10/11/2020
 *      Author: mlabadm
 */

#ifndef SRC_CAA_CTRL_H_
#define SRC_CAA_CTRL_H_

#include "comblock.h"


//u32 rule, fb_type, FIFO_READ;
//u32 sync, clear, set, init, test, reset;

void strPasIn(char *cmd); //TODO

void caa_setCfg(u32 rule, u32 fb_type, u32 sync, u32 clear, u32 set, u32 init, u32 test, u32 reset);

#endif /* SRC_CAA_CTRL_H_ */

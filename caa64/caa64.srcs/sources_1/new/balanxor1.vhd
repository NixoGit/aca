library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity balanxor1 is
    Port ( data_i : in STD_LOGIC_VECTOR (256-1 downto 0);
           sel_i  : in STD_LOGIC_VECTOR (  2-1 downto 0);
           en_i   : in STD_LOGIC;
           clk_i  : in STD_LOGIC;
           data_o : out STD_LOGIC_VECTOR (32-1 downto 0);
           en_o   : out STD_LOGIC);
end balanxor1;

architecture Behavioral of balanxor1 is
    signal data_r  : std_logic_vector(256-1 downto 0);
    signal data1_s : std_logic_vector(256-1 downto 0);
    signal data2_s : std_logic_vector(128-1 downto 0);
    signal data4_s : std_logic_vector( 64-1 downto 0);
    signal data8_s : std_logic_vector( 32-1 downto 0);
    signal en_r    : std_logic;
begin

    xor_p: process(clk_i)
    begin
        if rising_edge(clk_i)then
            data_r <= data_i;
            en_r <= en_i; 
        end if;
    end process xor_p;
    
    data1_s <= data_r;
    
    xor2_g: for i in 0 to 128-1 generate
        data2_s(i) <= data1_s(2*i+1) xor data1_s(2*i);
    end generate xor2_g;
    
    xor4_g: for i in 0 to 64-1 generate
        data4_s(i) <= data2_s(2*i+1) xor data2_s(2*i);
    end generate xor4_g;
    
    xor8_g: for i in 0 to 32-1 generate
        data8_s(i) <= data4_s(2*i+1) xor data4_s(2*i);
    end generate xor8_g;
    
    en_o   <= en_r;
    data_o <= data8_s(data_o'range) when sel_i = "11" else
              data4_s(data_o'range) when sel_i = "10" else
              data2_s(data_o'range) when sel_i = "01" else
              data1_s(data_o'range) ;
    
end Behavioral;

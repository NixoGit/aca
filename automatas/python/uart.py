import serial
import matplotlib.pyplot as plt

d=[]
with serial.Serial('/dev/ttyACM0', 115200, timeout=1) as ser:
    for i in range(4096):
        d.append(ser.readline())

plt.plot(d)
plt.show()
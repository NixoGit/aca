library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity balanxor is
   generic (
      WIDTH_I : positive:=128; -- Input Data width (bits)
      WIDTH_O : positive:=32;  -- Output Data width (bits)
      WSIZE   : positive:=4;   -- Max Window for Reduced Xor (bits)
      SSIZE   : positive:=4    -- Max Span of Window for Reduced Xor (bits)
   );
   port (
      -- inputs
      data_i :  in std_logic_vector(WIDTH_I-1 downto 0); -- Data Input
--      wsel_i :  in std_logic_vector(2-1 downto 0); -- Windows Select (ceil(log2((WSIZE-2)+1)))
--      ssel_i :  in std_logic_vector(2-1 downto 0); -- Span Select (ceil(log2((SSIZE-1)+1)))
      clk_i  :  in std_logic; -- Clock
      rst_i  :  in std_logic; -- Reset
      en_i   :  in std_logic; -- Enable
      -- outputs
      data_o : out std_logic_vector(WIDTH_O-1 downto 0);  -- Data Output
      en_o   : out std_logic -- Registered Enable 
   );
end entity balanxor;

--architecture ver_01 of balanxor is

--   function xor_reduce(s: std_logic_vector) return std_logic is
--      variable r: std_logic := '0';
--   begin
--      for i in s'range loop
--         r := r xor s(i);
--      end loop;
--      return r;
--   end function;
      
--   constant W0 : std_logic_vector(wsel_i'range) := (others => '0');
--   constant S0 : std_logic_vector(ssel_i'range) := (others => '0');
   
--   subtype t_dim1 is std_logic_vector(WIDTH_O-1 downto 0);
--   type    t_dim1_vector is array(natural range <>) of t_dim1;
--   subtype t_dim2 is t_dim1_vector(0 to SSIZE-1);
--   type    t_dim2_vector is array(natural range <>) of t_dim2;
--   subtype t_dim3 is t_dim2_vector(0 to WSIZE-2);

--   signal data_r   : std_logic_vector(data_i'range);
--   signal wsel_r   : std_logic_vector(wsel_i'range);
--   signal ssel_r   : std_logic_vector(ssel_i'range);
--   signal wsel_s   : std_logic_vector(wsel_i'range);
--   signal ssel_s   : std_logic_vector(ssel_i'range);
--   signal wsel_int : natural range 0 to WSIZE-2;
--   signal ssel_int : natural range 0 to SSIZE-1;
--   signal wsel_ind : natural range 0 to WSIZE-2;
--   signal ssel_ind : natural range 0 to SSIZE-1;
--   signal en_r     : std_logic;
--   signal xor_s    : t_dim3;   
   
--begin

--   wsel_int <= to_integer(unsigned(wsel_i));
--   ssel_int <= to_integer(unsigned(ssel_i));
--   wsel_s <= wsel_i when wsel_int<=WSIZE-2 else W0;
--   ssel_s <= ssel_i when ssel_int<=SSIZE-1 else S0;
--   wsel_ind <= to_integer(unsigned(wsel_r));
--   ssel_ind <= to_integer(unsigned(ssel_r));

--   reg_p: process (clk_i)
--   begin
--      if rising_edge(clk_i) then
--         if (rst_i='1') then
--            data_r <= (others => '0');
--            wsel_r <= std_logic_vector(to_unsigned(0,wsel_i'length));
--            ssel_r <= std_logic_vector(to_unsigned(0,ssel_i'length));
--            en_r   <= '0';
--         elsif (en_i='1') then
--            data_r <= data_i;
--            wsel_r <= wsel_s;
--            ssel_r <= ssel_s;
--            en_r   <= '1';
--         else
--            data_r <= data_r;
--            wsel_r <= wsel_r;
--            ssel_r <= ssel_r;
--            en_r   <= '0';
--         end if;
--      end if;
--   end process;
   
  
--   win_g: for w in 0 to WSIZE-2 generate
--   begin
--      -- -- Bad windows
--      -- winbad_g: if w<2 generate
--      -- begin
--      --    winbadspan_g: for s in 0 to SSIZE generate
--      --    begin
--      --       xor_s(w)(s) <= (others => '0');
--      --    end generate winbadspan_g;
--      -- end generate winbad_g;
--      -- Good windows
----      wingood_g: if w>=2 generate
----      begin
--         wingoodspan_g: for s in 0 to SSIZE-1 generate
--         begin
--            -- -- Bad spans
--            -- wingoodspanbad_g: if s<1 generate
--            -- begin
--            --    xor_s(w)(s) <= (others => '0');
--            -- end generate wingoodspanbad_g;
--            -- Good spans
----            wingoodspangood_g: if s>=1 generate
----            begin
--               -- XOR
--               wingoodspangoodxor_g: for b in 0 to (WIDTH_I-(w+2))/(s+1) generate
--               begin
--                  -- In output range
--                  xorgood_g: if b < WIDTH_O generate
--                  begin
--                     xor_s(w)(s)(b) <= xor_reduce(data_r((s+1)*b+(w+2)-1 downto (s+1)*b));
--                  end generate xorgood_g;
----                  -- out of output range
----                  xorbad_g: if b >= WIDTH_O-1 generate
----                  begin
----                     xor_s(w)(s)(b) <= '0';
----                  end generate xorbad_g;
--               end generate wingoodspangoodxor_g;
----            end generate wingoodspangood_g;
--         end generate wingoodspan_g;
----      end generate wingood_g;
--   end generate win_g;
   
--   data_o <= xor_s(wsel_ind)(ssel_ind);
--   en_o   <= en_r;

--end architecture ver_01;

architecture ver_02 of balanxor is

   function xor_reduce(s: std_logic_vector) return std_logic is
      variable r: std_logic := '0';
   begin
      for i in s'range loop
         r := r xor s(i);
      end loop;
      return r;
   end function;
      
--   constant W0 : std_logic_vector(wsel_i'range) := (others => '0');
--   constant S0 : std_logic_vector(ssel_i'range) := (others => '0');
   
   subtype t_dim1 is std_logic_vector(WIDTH_O-1 downto 0);
--   type    t_dim1_vector is array(natural range <>) of t_dim1;
--   subtype t_dim2 is t_dim1_vector(0 to SSIZE-1);
--   type    t_dim2_vector is array(natural range <>) of t_dim2;
--   subtype t_dim3 is t_dim2_vector(0 to WSIZE-2);

   signal data_r   : std_logic_vector(data_i'range);
--   signal wsel_r   : std_logic_vector(wsel_i'range);
--   signal ssel_r   : std_logic_vector(ssel_i'range);
--   signal wsel_s   : std_logic_vector(wsel_i'range);
--   signal ssel_s   : std_logic_vector(ssel_i'range);
--   signal wsel_int : natural range 0 to WSIZE-2;
--   signal ssel_int : natural range 0 to SSIZE-1;
--   signal wsel_ind : natural range 0 to WSIZE-2;
--   signal ssel_ind : natural range 0 to SSIZE-1;
   signal en_r     : std_logic;
   signal xor_s    : t_dim1;   
   
begin

--   wsel_int <= to_integer(unsigned(wsel_i));
--   ssel_int <= to_integer(unsigned(ssel_i));
--   wsel_s <= wsel_i when wsel_int<=WSIZE-2 else W0;
--   ssel_s <= ssel_i when ssel_int<=SSIZE-1 else S0;
--   wsel_ind <= to_integer(unsigned(wsel_r));
--   ssel_ind <= to_integer(unsigned(ssel_r));

   reg_p: process (clk_i)
   begin
      if rising_edge(clk_i) then
         if (rst_i='1') then
            data_r <= (others => '0');
--            wsel_r <= std_logic_vector(to_unsigned(0,wsel_i'length));
--            ssel_r <= std_logic_vector(to_unsigned(0,ssel_i'length));
            en_r   <= '0';
         elsif (en_i='1') then
            data_r <= data_i;
--            wsel_r <= wsel_s;
--            ssel_r <= ssel_s;
            en_r   <= '1';
         else
            data_r <= data_r;
--            wsel_r <= wsel_r;
--            ssel_r <= ssel_r;
            en_r   <= '0';
         end if;
      end if;
   end process;
   
  
    wingoodspangoodxor_g: for b in 0 to (128-4)/(4) generate
    begin
        xor_s(b) <= xor_reduce(data_r(4*(b+1)-1 downto 4*b));
    end generate wingoodspangoodxor_g;
   
   data_o <= xor_s;
   en_o   <= en_r;

end architecture ver_02;
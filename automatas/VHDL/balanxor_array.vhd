library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity balancexor_array is
  generic(ncells : integer:=256;
          num_xor : integer:=8
        --   data_size : integer :=ncells/num_xor
            );
  Port (data_i : in STD_LOGIC_VECTOR (ncells-1 downto 0);
        dvalid_i : in STD_LOGIC;
        en_i : in STD_LOGIC;
        xor_mask : in STD_LOGIC_VECTOR((ncells/num_xor)-1 downto 0);
        sel_i  : in STD_LOGIC_VECTOR(1 DOWNTO 0);
        clk_i  : in STD_LOGIC;
        rst_n  : in STD_LOGIC;
        data_o : out STD_LOGIC_VECTOR ((ncells/num_xor)-1 downto 0);
        dvalid_o   : out STD_LOGIC;
        dxor_dbg_o : out STD_LOGIC_VECTOR((ncells/num_xor) -1 downto 0)
           );
end balancexor_array ; 

architecture Behavioral of balancexor_array is

    constant data_after_xor : integer := ncells/num_xor;
    constant data_size : integer :=ncells/num_xor;
    constant buff_len : integer:=data_size/num_xor;
    

    type dbuff is array (buff_len -1 downto 0) of std_logic_vector(data_size -1 downto 0);
    signal din_buff, dout_buff : dbuff;

    signal dxor : std_logic_vector(data_after_xor -1 downto 0);

    signal din_counter, dout_counter : integer;

    signal data_o_sig :std_logic_vector(data_size -1 downto 0);
    signal dvalid_o_sig, dvalid_i_sig : std_logic;

    signal data_i_signal : STD_LOGIC_VECTOR (ncells-1 downto 0);

    component nbitxor is
        generic(N : integer:=num_xor);
        port (
          din : in std_logic_vector(N-1 downto 0);
          dout : out std_logic
        ) ;
      end component; 

begin

    process(clk_i, rst_n)
    begin
        if rst_n='0' then
            data_i_signal<=(others=>'0');
            dvalid_i_sig<='0';
        elsif rising_edge(clk_i) then
            if dvalid_i = '1' then
                data_i_signal<=data_i;
                dvalid_i_sig<='1';
            else
                data_i_signal<=data_i_signal;
                dvalid_i_sig<='0';
            end if;
        end if;
    end process;

    -- Doing n-bits xor operation
    -- gen_xor: for i in 1 to data_after_xor generate
    --     dxor(i-1)<=xor data_i((i*num_xor)-1 downto num_xor*(i-1)); 
    -- end generate;
    gen_xor: for i in 1 to data_after_xor generate
        i_xor: nbitxor
        port map (
            din => data_i_signal((i*num_xor)-1 downto num_xor*(i-1)),
            dout =>dxor(i-1)
        );
    end generate;

    dxor_dbg_o<=dxor;


    process(clk_i, rst_n)
        variable d_count : integer range 0 to buff_len;
    begin
        if (rst_n = '0') then
            din_counter<=0;
            d_count:=0;
        elsif rising_edge(clk_i) then
            if en_i= '0' then
                din_counter<=0;
                d_count:=0;
            elsif (dvalid_i = '1' and en_i = '1') then
                din_buff(d_count)<=dxor;
                din_counter<=d_count;
                d_count:=d_count+1;
                if (d_count >= buff_len) then
                    d_count:=0;    
                end if;
            end if;
        end if;
    end process;
    
    --Transposing the output
    dout_generate: for i in 1 to buff_len generate
        din_generate : for j in 1 to buff_len generate
            dout_buff(i-1)((j*num_xor)-1 downto num_xor*(j-1))<=din_buff(j-1)((i*num_xor)-1 downto num_xor*(i-1));
        end generate; -- din_generate
    end generate; -- dout_generate

    process (rst_n, clk_i, din_counter, en_i)
        variable i: integer range 0 to buff_len;
    begin
        if rst_n='0' then
            i:=0;
            dvalid_o_sig<='0';
            data_o_sig<=(others=>'0');
        elsif rising_edge(clk_i) then
            if en_i ='0' then
                i:=0;
                dvalid_o_sig<='0';
                data_o_sig<=(others=>'0'); 
            elsif din_counter=buff_len-1 and i<buff_len then
                data_o_sig<=dout_buff(i);
                dvalid_o_sig<='1';
                i:=i+1;
            elsif din_counter = 0 then
                i:=0;
                dvalid_o_sig<='0';
            else
                dvalid_o_sig<='0';
            end if;
        end if;
    end process;

    data_o<=    data_o_sig                                   when sel_i = "01" else 
                dxor                                         when sel_i = "10" else
                data_i_signal(data_size -1 downto 0) xor xor_mask when sel_i = "11" else
                data_i(data_size -1 downto 0); 
    
    dvalid_o<=  (dvalid_o_sig and en_i) when sel_i ="01" else
                (dvalid_o_sig and en_i) when sel_i ="10" else
                (dvalid_i_sig and en_i);
    

end Behavioral;
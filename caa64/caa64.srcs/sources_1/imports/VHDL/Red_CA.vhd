library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Red_CA is
GENERIC	(N_cel : INTEGER :=5; i_N: INTEGER :=5); --number of forced delay buffers
PORT (
Salida: OUT STD_LOGIC_vector(N_cel-1 downto 0);
Salida_raw: OUT STD_LOGIC_vector(N_cel-1 downto 0);
Rule : IN STD_LOGIC_vector(2**i_N-1 downto 0);
fb_type : IN STD_LOGIC_vector(2 downto 0);
raw_type : IN STD_LOGIC_vector(2 downto 0);
clk : IN STD_LOGIC;
sync : IN STD_LOGIC;
clear : IN STD_LOGIC;
sett : IN STD_LOGIC;
init : IN STD_LOGIC;
clk_carga : IN STD_LOGIC;
carga_en   : IN STD_LOGIC;
test : IN STD_LOGIC;
reset : IN STD_LOGIC
);
end Red_CA;

architecture Behavioral of Red_CA is
type entrada_CA is array (N_cel-1 downto 0) of std_logic_vector(i_N-1 downto 0);
signal ent : entrada_CA;

signal  LUT1_OUT,reset_i:STD_LOGIC;
signal  sal_cel:STD_LOGIC_vector(N_cel-1 downto 0);
signal  sal_raw:STD_LOGIC_vector(N_cel-1 downto 0);
signal  i_clear:STD_LOGIC;
signal Regla:STD_LOGIC_vector(31 downto 0);

signal counter : unsigned(N_cel-1 downto 0);
constant cmax : unsigned(N_cel-1 downto 0):=(others=>'1');

signal carga_sig : STD_LOGIC_VECTOR(N_cel-1 downto 0);

  
component celula
GENERIC	(i_N : INTEGER :=5); --number of forced delay buffers
PORT (
Salida: OUT STD_LOGIC;
Salida_raw: OUT STD_LOGIC;
Entrada : IN STD_LOGIC_vector(i_N-1 downto 0);
Rule : IN STD_LOGIC_vector(2**i_N-1 downto 0);
fb_type : IN STD_LOGIC_vector(2 downto 0);
raw_type : IN STD_LOGIC_vector(2 downto 0);
clk : IN STD_LOGIC;
clk_carga : IN STD_LOGIC;
sync : IN STD_LOGIC;
n_clear : IN STD_LOGIC;
set : IN STD_LOGIC;
init : IN STD_LOGIC;
reset : IN STD_LOGIC;
carga : IN STD_LOGIC;
carga_serie_o: OUT STD_LOGIC
);
end component;

begin

process(clk, reset)
begin
    if reset ='1' then
        counter<=(others=>'0');
     elsif rising_edge(clk) then
        if counter=cmax then
            counter<=(others=>'0');
        else
            counter<=counter+1;
        end if;
     end if;
end process;

i_clear<=not(clear);
--Regla<="10101001100110101001101010100101";
Regla<=Rule;
ent(0)<=sal_cel(N_cel-2)&sal_cel(N_cel-1)&sal_cel(0)&sal_cel(1)&sal_cel(2);
   celula0: celula 
    GENERIC	MAP	(i_N =>i_N) 
	PORT MAP (Salida=>sal_cel(0),Salida_raw=>sal_raw(0),Entrada =>ent(0),Rule =>Regla,fb_type=>fb_type,raw_type=>raw_type,clk=>clk,clk_carga=>clk_carga,sync=>sync,n_clear=>i_clear,set=>sett,init=>init,reset=>reset, carga=>carga_en, carga_serie_o=>carga_sig(0));
ent(1)<=sal_cel(N_cel-1)&sal_cel(0)&sal_cel(1)&sal_cel(2)&sal_cel(3);
celula1: celula 
    GENERIC	MAP	(i_N =>i_N)
	PORT MAP (Salida=>sal_cel(1),Salida_raw=>sal_raw(1),Entrada =>ent(1),Rule =>Regla,fb_type=>fb_type,raw_type=>raw_type,clk=>clk,clk_carga=>clk_carga,sync=>sync,n_clear=>i_clear,set=>sett,init=>carga_sig(0),reset=>reset, carga=>carga_en, carga_serie_o=>carga_sig(1));
	
  g_GENERATE_FOR: for ii in 2 to N_cel-3 generate
  begin
  ent(ii)<=sal_cel(ii-2)&sal_cel(ii-1)&sal_cel(ii)&sal_cel(ii+1)&sal_cel(ii+2);
inst_celula : celula 
    GENERIC	MAP	(i_N =>i_N) 
	PORT MAP (Salida=>sal_cel(ii),Salida_raw=>sal_raw(ii),Entrada =>ent(ii),Rule =>Regla,fb_type=>fb_type,raw_type=>raw_type,clk=>clk,clk_carga=>clk_carga,sync=>sync,n_clear=>i_clear,set=>sett,init=>carga_sig(ii-1),reset=>reset, carga=>carga_en, carga_serie_o=>carga_sig(ii));
  end generate g_GENERATE_FOR;

ent(N_cel-2)<=sal_cel(0)&sal_cel(N_cel-1)&sal_cel(N_cel-2)&sal_cel(N_cel-3)&sal_cel(N_cel-4);
   celulaNm2: celula 
    GENERIC	MAP	(i_N=>i_N) 
	PORT MAP (Salida=>sal_cel(N_cel-2),Salida_raw=>sal_raw(N_cel-2),Entrada=>ent(N_cel-2),Rule =>Regla,fb_type=>fb_type,raw_type=>raw_type,clk=>clk,clk_carga=>clk_carga,sync=>sync,n_clear=>i_clear,set=>sett,init=>carga_sig(N_cel-3),reset=>reset, carga=>carga_en, carga_serie_o=>carga_sig(N_cel-2));

ent(N_cel-1)<=sal_cel(N_cel-3)&sal_cel(N_cel-2)&sal_cel(N_cel-1)&sal_cel(0)&sal_cel(1);
celulaNm1: celula 
    GENERIC	MAP	(i_N=>i_N) 
	PORT MAP (Salida=>sal_cel(N_cel-1),Salida_raw=>sal_raw(N_cel-1),Entrada =>ent(N_cel-1),Rule =>Regla,fb_type=>fb_type,raw_type=>raw_type,clk=>clk,clk_carga=>clk_carga,sync=>sync,n_clear=>i_clear,set=>sett,init=>carga_sig(N_cel-2),reset=>reset, carga=>carga_en, carga_serie_o=>carga_sig(N_cel-1));


Salida<=sal_cel when test='0' else std_logic_vector(counter);
Salida_raw<=sal_raw when test='0' else std_logic_vector(counter);

end Behavioral;

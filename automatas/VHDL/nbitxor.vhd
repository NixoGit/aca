library ieee ;
    use ieee.std_logic_1164.all ;
    use ieee.numeric_std.all ;

entity nbitxor is
  generic(N : integer:=8);
  port (
    din : in std_logic_vector(N-1 downto 0);
    dout : out std_logic
  ) ;
end nbitxor ; 

architecture Behavioral  of nbitxor is
    signal temp: std_logic_vector(N-1 downto 0);
begin

    temp(0)<=din(0);
    gen_xor: for i in 1 to N-1 generate
        temp(i)<=temp(i-1) xor din(i);
    end generate;
    dout<=temp(N-1);
end Behavioral ;
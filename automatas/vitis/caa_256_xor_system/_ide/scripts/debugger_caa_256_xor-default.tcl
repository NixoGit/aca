# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: /opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_xor_system/_ide/scripts/debugger_caa_256_xor-default.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source /opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_xor_system/_ide/scripts/debugger_caa_256_xor-default.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Zed 210248685704" && level==0 && jtag_device_ctx=="jsn-Zed-210248685704-23727093-0"}
fpga -file /opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_xor/_ide/bitstream/caa_256_balanxor1_base_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw /opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_balanxor1_base_wrapper/export/caa_256_balanxor1_base_wrapper/hw/caa_256_balanxor1_base_wrapper.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source /opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_xor/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow /opt/my_gits/AUTOMATAS/automatas/Vitis/caa_256_xor/Debug/caa_256_xor.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "*A9*#0"}
con

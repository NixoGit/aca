library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity balanx is
    Port ( data_i : in  STD_LOGIC_VECTOR (1283-1 downto 0);
           en_i   : in  STD_LOGIC;
           seli_i : in  STD_LOGIC_VECTOR (   3-1 downto 0);
           selo_i : in  STD_LOGIC_VECTOR (   2-1 downto 0);
           clk_i  : in  STD_LOGIC;
           data_o : out STD_LOGIC_VECTOR (  32-1 downto 0);
           en_o   : out STD_LOGIC);
end balanx;

architecture Behavioral of balanx is
    constant NB : natural := 256;
    subtype b_t is std_logic_vector(NB-1 downto 0);
    type    m_t is array(5-1 downto 0) of b_t;
    signal datam_s : m_t;
    signal seli_ind : natural;
    signal selo_ind : natural;
    signal data_s  : std_logic_vector(NB/1-1 downto 0);
    signal data_r  : std_logic_vector(NB/1-1 downto 0);
    signal data1_s : std_logic_vector(NB/1-1 downto 0);
    signal data2_s : std_logic_vector(NB/2-1 downto 0);
    signal data4_s : std_logic_vector(NB/4-1 downto 0);
    signal data8_s : std_logic_vector(NB/8-1 downto 0);
    signal en_r    : std_logic;
begin

    mux_g: for i in datam_s'range(1) generate
    begin
        datam_s(i)(0) <= data_i(0) ;
        bit_g: for j in datam_s(0)'high downto 1 generate
        begin
            datam_s(i)(j) <= data_i((i+1)*j) ;
        end generate bit_g;
    end generate mux_g;

    seli_ind <= to_integer(unsigned(seli_i)) when to_integer(unsigned(seli_i))<5 else 0;
    selo_ind <= to_integer(unsigned(selo_i)) when to_integer(unsigned(selo_i))<4 else 0;

    data_s <= datam_s(seli_ind)(data_s'range) ;

    xor_p: process(clk_i)
    begin
        if rising_edge(clk_i)then
            data_r <= data_s;
            en_r <= en_i; 
        end if;
    end process xor_p;
    
    data1_s <= data_r;
    
    xor2_g: for i in data2_s'range generate
        data2_s(i) <= data1_s(2*i+1) xor data1_s(2*i);
    end generate xor2_g;
    
    xor4_g: for i in data4_s'range generate
        data4_s(i) <= data2_s(2*i+1) xor data2_s(2*i);
    end generate xor4_g;
    
    xor8_g: for i in data8_s'range generate
        data8_s(i) <= data4_s(2*i+1) xor data4_s(2*i);
    end generate xor8_g;
    
    en_o    <=  en_r;
    data_o  <=  data8_s(data_o'range) when selo_ind=3 else
                data4_s(data_o'range) when selo_ind=2 else
                data2_s(data_o'range) when selo_ind=1 else
                data1_s(data_o'range) ;
    
end Behavioral;

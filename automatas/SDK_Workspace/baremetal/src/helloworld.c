/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include <unistd.h>
#include "comblock.h"
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"


u32 data;
u32 rule;



int main()
{
    init_platform();

    //write init values in fifo:
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OFIFO_CONTROL, 1);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OFIFO_CONTROL, 0);
    for (int i=0; i<1000; i++){
    	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OFIFO_VALUE, i);
    }

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 1);
    usleep(100000);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 0);
//    for (rule=2845481637; rule<0xffffffff; rule++){
//    rule=2845481637; 	// Golden Rule
//    rule=1771476585; 	// new rule 1
//    rule=2523490710;  // new rule 2
//    rule=515788723; 	// Ligeramente desbalanceada. Muere en 3.7us
//    rule=1771476585; 	// New Golden Rule 1
//    rule=2523490710; 	// New Golden Rule 2

    rule=2875481639;

    xil_printf("Rule set: %x\n\r",rule);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG9, 0); 			// Test Enable
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 0); 			// Stop FIFO write
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG8, rule); 		// Set Rule
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG0, 6);  			// Set fb_type
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG1, 0);  			// Set sync
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG2, 0);  			// Clear
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG3, 0);  			// Set
//    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG4, 0);  		// Init

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 1); 			// reset cell array
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 0);
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 1); 	// Clear FIFO
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 0);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 1); 			// Enable FIFO write


    for(int i=0; i<100000; i++){
//      usleep(5000);
//      cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 1); //reset cell array
//		usleep(5000);
    	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 0);
    }

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 0);
    for (int i=0; i<31250000; i++){
    	data=cbRead(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_VALUE);
    	xil_printf("%U\n\r",data);
    }

    cleanup_platform();
    return 0;
}

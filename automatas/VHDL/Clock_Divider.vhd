----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/29/2020 12:20:02 PM
-- Design Name: 
-- Module Name: Clock_Divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

entity Clock_Divider is
port ( 
    clk_i:      in      std_logic;
    rst_i:      in      std_logic;
    clk_o:      out     std_logic := '0';
    clk_div:    in      std_logic_vector(31 downto 0)
);
end Clock_Divider;
  
architecture Behavioral of Clock_Divider is
  
signal count: unsigned(31 downto 0):= (others => '0');
signal tmp : std_logic := '0';
  
begin
  
process(clk_i, rst_i)
begin
    if rising_edge(clk_i) then
        if(rst_i = '1') then -- change reset to synchronous
		    count <= (others => '0');
		    tmp <= '1';
        else
            if count = unsigned(clk_div) - 1 then
                tmp <= not tmp;        
                count <= (others => '0');
            else
                count <= count + 1;
            end if;
        end if;
	end if;
end process;

clk_o <= tmp;

end Behavioral;

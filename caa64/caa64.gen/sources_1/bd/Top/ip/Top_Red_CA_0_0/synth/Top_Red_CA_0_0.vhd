-- (c) Copyright 1995-2023 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:module_ref:Red_CA:1.0
-- IP Revision: 1

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY Top_Red_CA_0_0 IS
  PORT (
    Salida : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    Salida_raw : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    Rule : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    fb_type : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    raw_type : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    clk : IN STD_LOGIC;
    sync : IN STD_LOGIC;
    clear : IN STD_LOGIC;
    sett : IN STD_LOGIC;
    init : IN STD_LOGIC;
    clk_carga : IN STD_LOGIC;
    carga_en : IN STD_LOGIC;
    test : IN STD_LOGIC;
    reset : IN STD_LOGIC
  );
END Top_Red_CA_0_0;

ARCHITECTURE Top_Red_CA_0_0_arch OF Top_Red_CA_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF Top_Red_CA_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT Red_CA IS
    GENERIC (
      N_cel : INTEGER;
      i_N : INTEGER
    );
    PORT (
      Salida : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      Salida_raw : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
      Rule : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      fb_type : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      raw_type : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      clk : IN STD_LOGIC;
      sync : IN STD_LOGIC;
      clear : IN STD_LOGIC;
      sett : IN STD_LOGIC;
      init : IN STD_LOGIC;
      clk_carga : IN STD_LOGIC;
      carga_en : IN STD_LOGIC;
      test : IN STD_LOGIC;
      reset : IN STD_LOGIC
    );
  END COMPONENT Red_CA;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF Top_Red_CA_0_0_arch: ARCHITECTURE IS "Red_CA,Vivado 2022.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF Top_Red_CA_0_0_arch : ARCHITECTURE IS "Top_Red_CA_0_0,Red_CA,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF Top_Red_CA_0_0_arch: ARCHITECTURE IS "Top_Red_CA_0_0,Red_CA,{x_ipProduct=Vivado 2022.1,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=Red_CA,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED,N_cel=64,i_N=5}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF Top_Red_CA_0_0_arch: ARCHITECTURE IS "module_ref";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF clk: SIGNAL IS "XIL_INTERFACENAME clk, ASSOCIATED_RESET reset, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN Top_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF clk: SIGNAL IS "xilinx.com:signal:clock:1.0 clk CLK";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset: SIGNAL IS "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF reset: SIGNAL IS "xilinx.com:signal:reset:1.0 reset RST";
BEGIN
  U0 : Red_CA
    GENERIC MAP (
      N_cel => 64,
      i_N => 5
    )
    PORT MAP (
      Salida => Salida,
      Salida_raw => Salida_raw,
      Rule => Rule,
      fb_type => fb_type,
      raw_type => raw_type,
      clk => clk,
      sync => sync,
      clear => clear,
      sett => sett,
      init => init,
      clk_carga => clk_carga,
      carga_en => carga_en,
      test => test,
      reset => reset
    );
END Top_Red_CA_0_0_arch;

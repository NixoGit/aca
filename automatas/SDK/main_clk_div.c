/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */
/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "comblock.h"
#include "xparameters.h"
#include <unistd.h>


u32 data;
u32 rule;



int main()
{
    init_platform();

//write init values in fifo:
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG10, 16); //Reduce clock frequency by factor of 2

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OFIFO_CONTROL, 1);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OFIFO_CONTROL, 0);
    for (int i=0; i<1000; i++){
    	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OFIFO_VALUE, i%2);
    }

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 1);
    usleep(100000);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 0);
//    for (rule=2845481637; rule<0xffffffff; rule++){
//    rule=2845481637;
    rule=2845481637;
    xil_printf("Rule set: %x\n\r",rule);

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG9, 0); //Test Enable

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 0); //Stop FIFO write


    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG8, rule); //Set Rule
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG0, 6);  //Set fb_type
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG1, 0);  //Set sync
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG2, 0);  //Clear
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG3, 0);  //Set
//    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG4, 0);  //Init

    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 1); //reset cell array
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 0);

	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 1); //Clear FIFO
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 0);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 1); //Enable FIFO write

    for(int i=0; i<10; i++){
    usleep(100000);
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 1); //reset cell array
    usleep(100000);
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG5, 0);
    }
    for (int i=0; i<2048; i++){
    	data=cbRead(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_VALUE);
    	xil_printf("%d\n\r",data);
   }
//    }

    cleanup_platform();
    return 0;
}
